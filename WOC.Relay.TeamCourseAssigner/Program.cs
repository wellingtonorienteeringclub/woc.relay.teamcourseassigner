﻿using IOF.XML.V3;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace WOC.Relay.TeamCourseAssigner {
  class Program {
    static void Main(string[] args) {
      /*
      var settings = new Settings {
        Grades = new List<EventSettings> {
          new EventSettings {
            BibPrefix = "E",
            Legs = 4,
            Name = "Elites",
            NumberOfTeams = 16,
            StartNumber = 1
          },
          new EventSettings {
            BibPrefix = "S",
            Legs = 4,
            Name = "Seniors",
            NumberOfTeams = 20,
            StartNumber = 101
          }
        }
      };

      File.WriteAllText("settings.json", JsonConvert.SerializeObject(settings, Formatting.Indented));
      */

      var settings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText("settings.json"));

      var tca = new List<TeamCourseAssignment>();

      foreach (var g in settings.Grades) {
        var variations = Randomize(g.Variations).ToList();


        for (var i = 0; i < g.NumberOfTeams; i++) {
          var tmca = new List<TeamMemberCourseAssignment>();

          for (var j = 1; j <= g.Legs; j++) {
            var courseName = variations[i][j - 1];

            tmca.Add(new TeamMemberCourseAssignment {
              BibNumber = $"{g.StartNumber + i}-{j}",
              CourseFamily = g.Name,
              CourseName = courseName,
              TeamMemberName = "",
              Leg = $"{j}"
            });
          }

          tca.Add(new TeamCourseAssignment {
            BibNumber = $"{g.StartNumber + i}",
            TeamName = $"{g.StartNumber + i}",
            TeamMemberCourseAssignment = tmca.ToArray(),
            ClassName = $"{g.Name}",
          });
        }
      }

      var cd = new CourseData {
        CreateTime = DateTime.Now,
        IofVersion = "3.0",
        CreateTimeSpecified = true,
        Creator = "WOC",
        Event = new Event {
          Name = "Relay"
        },
        RaceCourseData = new List<RaceCourseData> { new RaceCourseData {
          TeamCourseAssignment = tca.ToArray()
        } }.ToArray()
      };

      using (var file = File.Create("relay.xml")) {
        new XmlSerializer(typeof(CourseData)).Serialize(file, cd);
      }

      Console.WriteLine("Done!");
      Console.ReadLine();
    }

    public static IEnumerable<T> Randomize<T>(IEnumerable<T> source) {
      Random rnd = new Random();
      return source.OrderBy<T, int>((item) => rnd.Next());
    }
  }

  internal class EventSettings {
    public string Name { get; set; }
    public string BibPrefix { get; set; }
    public int StartNumber { get; set; }
    public int NumberOfTeams { get; set; }
    public int Legs { get; set; }
    public List<List<string>> Variations { get; set; }
  }

  internal class Settings {
    public List<EventSettings> Grades { get; set; }
  }
}
